<?php
/*
External Log Interface
by Steinsplitter

Debug:
error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|
ini_set('display_errors', 'On');
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
        <meta http-equiv="Content-type" content="text/html;charset=UTF-8" />
        <title>Commons Delinker</title>
        <link href="//tools-static.wmflabs.org/cdnjs/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
        <script src="//tools-static.wmflabs.org/tooltranslate/tt.js"></script>
        <script src="//tools-static.wmflabs.org/cdnjs/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <script src="https://tools-static.wmflabs.org/cdnjs/ajax/libs/twitter-bootstrap/4.5.2/js/bootstrap.min.js"></script>
  </style>
<script>
        $(document).on('click', '#xcdbutton', function() {
        $('#xdelinkerloader').show();
        });

</script>
</head>
<body>
      <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
        <h5 class='my-0 mr-md-auto font-weight-normal'><span tt="delinquent">Commons file delinking log viewer</span></h5>
         <nav class="my-2 my-md-0 mr-md-3">
          <a class="p-2 text-dark" href="index.php" id="cdbutton"><span class="glyphicon glyphicon-road"></span> Commons Delinker LOG</a>
        </nav>
    </div>


<div class="container">

<div id='xdelinkerloader' style='display:none'><center>
<div class='d-flex justify-content-center'>
  <div class='spinner-border' role='status'>
  </div>
</div>
<b>Loading...</b></center></div>

<div class='lead'>
<h1>Delinks by other Bots</h1><p>Delinks not performed by CommonsDelinker but with a helper bot (not operated by the CommonsDelinker bot operators).<br><small>This tool is experimental, please check the original log or bots contributions for exact results.</small><br><br>
</div>
<form action="/x-delinkers.php" method="get">
  <div class="form-row align-items-center">
    <div class="col-auto">
      <label class="sr-only" for="inlineFormInputGroup">File</label>
      <div class="input-group mb-2">
        <div class="input-group-prepend">
          <div class="input-group-text">File:</div>
        </div>
        <input type="text" class="form-control" id="inlineFormInputGroup" name="file" value="<?php echo htmlspecialchars($_GET["file"]); ?>">
      </div>
    </div>
    <div class="col-auto">
      <button type="submit" id="xcdbutton" class="btn btn-primary mb-2">Search</button>
    </div>
  </div>
</form>

<table class="table table-condensed table-striped">
<thead><tr class="bg-secondary text-white"><th>File</th><th>Wiki</th><th>Bot</th><th>Page/Diff</th></tr></thead>
<tbody style="font-size:9pt"><tr>
<?php
$xdata = "no";
if (empty($_GET['file'])) {
 echo '<div class="alert alert-info" role="alert"><span tt="naoresult">Please enter a filename.</span></div>';
 die();
}

 $fn = fopen("/data/project/krdbot/Delinker.log","r");

  while(! feof($fn))  {
        $result = fgets($fn);
//$a = ';[[:File:Mothman statue in West Virigina.jpg]];';
$searchfile = $_GET["file"];
$a = ';[[:File:'. $searchfile .']];';

if (strpos($result, $a) !== false) {
//echo $result;
preg_match('/[0-9]*;\[\[:File:(.*?)\]\];\[\[.*;.*([a-z]{2,8}\.(wikipedia|wikimedia)\.org);\[\[:(.*)\]\];(.*)/', $result, $datx );
//var_dump($datx);
//echo $result;
//echo "<br>";
$xdata = "yes";
if ($datx[2] === "de.wikipedia.org") {
  $botx = "Dateientlinkerbot";
} else if ($datx[2] === "en.wikipedia.org") {
  $botx = "Filedelinkerbot";
} else  {
  $botx = "Filedelinkerbot";
}
  echo "<tr><td>File:" . htmlspecialchars($datx[1]) . "</td> <td>". $datx[2] . "</td> <td>". $botx ."</td> <td><a href='https://". $datx[2] . "/wiki/Special:Diff/". $datx[5]  ."'>". $datx[4]  ."</a></td></tr>";;
}
  }

  fclose($fn);

echo "</table>";
if ($xdata === "no") {
  echo '<div class="alert alert-danger" role="alert"><span tt="noresult">No results.</span></div>';
}
?>
</div>
</div>
</div>
</body>
</html>
